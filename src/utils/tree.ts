/**
 * @author Di Wang
 */
import { MenuEntity } from '../models/menu.model';

/**
 * Only update the children attribute of a tree node.
 * Since the menu tree isn't affect rendering, there is only a shadow copy
 * @param root
 * @param item
 */
export function updateTreeItemChildren(root: MenuEntity[], item: MenuEntity): MenuEntity[] {
  const ids = item.id.split('-').map((item) => parseInt(item));

  let target: MenuEntity = item;
  let curr: MenuEntity[] | null = root;
  for (const id of ids) {
    if (curr) {
      target = curr[id];
      curr = target.children;
    }
  }
  target.children = item.children;

  return root;
}

/**
 * Get the tree children nodes by the parent id
 * @param root
 * @param parentId
 */
export function getTreeChildren(root: MenuEntity[], parentId: string): MenuEntity[] | null {
  const ids = parentId.split('-').map((item) => parseInt(item));

  let target: MenuEntity | null = null;
  let curr: MenuEntity[] | null = root;
  for (const id of ids) {
    if (curr) {
      target = curr[id];
      curr = target.children;
    }
  }

  return target ? target.children : null;
}
