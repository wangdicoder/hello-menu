/**
 * @author Di Wang
 */
import { updateTreeItemChildren, getTreeChildren } from '../tree';
import { MenuEntity } from '../../models/menu.model';

describe('Tree Utils', () => {
  it('should update tree children correctly', () => {
    const rootMenu: MenuEntity[] = [
      {
        id: '0',
        name: 'Menu 1',
        path: '/menu1',
        type: 'menu',
        parentId: null,
        children: null,
      },
    ];
    const target: MenuEntity = {
      id: '0',
      name: 'Menu 1',
      path: '/menu1',
      type: 'menu',
      parentId: null,
      children: [
        {
          id: '0-0',
          name: 'Menu 1',
          path: '/menu1',
          type: 'form',
          parentId: '0',
          children: null,
        },
      ],
    };
    expect(updateTreeItemChildren(rootMenu, target)).toStrictEqual([target]);
  });

  it('should update deeper tree correctly', () => {
    const rootMenu: MenuEntity[] = [
      {
        id: '0',
        name: 'Menu 1',
        path: '/menu1',
        type: 'menu',
        parentId: null,
        children: [
          {
            id: '0-0',
            name: 'Menu 1',
            path: '/menu1',
            type: 'form',
            parentId: '0',
            children: [
              {
                id: '0-0-0',
                name: 'Menu 1',
                path: '/menu1',
                type: 'null',
                parentId: '0-0',
                children: null,
              },
              {
                id: '0-0-1',
                name: 'Menu 2',
                path: '/menu2',
                type: 'form',
                parentId: '0-0',
                children: null,
              },
            ],
          },
        ],
      },
    ];
    const target: MenuEntity = {
      id: '0-0-0',
      name: 'Menu 1',
      path: '/menu1',
      type: 'null',
      parentId: '0-0',
      children: [
        {
          // Diff
          id: '0-0-0-0',
          name: 'Menu 1',
          path: '/menu1',
          type: 'null',
          parentId: '0-0-0',
          children: null,
        },
      ],
    };
    expect(updateTreeItemChildren(rootMenu, target)).toStrictEqual([
      {
        id: '0',
        name: 'Menu 1',
        path: '/menu1',
        type: 'menu',
        parentId: null,
        children: [
          {
            id: '0-0',
            name: 'Menu 1',
            path: '/menu1',
            type: 'form',
            parentId: '0',
            children: [
              {
                id: '0-0-0',
                name: 'Menu 1',
                path: '/menu1',
                type: 'null',
                parentId: '0-0',
                children: [
                  {
                    // Diff
                    id: '0-0-0-0',
                    name: 'Menu 1',
                    path: '/menu1',
                    type: 'null',
                    parentId: '0-0-0',
                    children: null,
                  },
                ],
              },
              {
                id: '0-0-1',
                name: 'Menu 2',
                path: '/menu2',
                type: 'form',
                parentId: '0-0',
                children: null,
              },
            ],
          },
        ],
      },
    ]);
  });

  it('should get correct tree item', () => {
    const rootMenu: MenuEntity[] = [
      {
        id: '0',
        name: 'Menu 1',
        path: '/menu1',
        type: 'menu',
        parentId: null,
        children: [
          {
            id: '0-0',
            name: 'Menu 1',
            path: '/menu1',
            type: 'form',
            parentId: '0',
            children: null,
          },
        ],
      },
    ];
    expect(getTreeChildren(rootMenu, '0')).toStrictEqual([
      {
        id: '0-0',
        name: 'Menu 1',
        path: '/menu1',
        type: 'form',
        parentId: '0',
        children: null,
      },
    ]);
    expect(getTreeChildren(rootMenu, '0-0')).toStrictEqual(null);
  });
});
