/**
 * @author Di Wang
 */
import { createStore, combineReducers } from 'redux';
import { devToolsEnhancer } from 'redux-devtools-extension/developmentOnly';
import menuReducer from './redux/menu.redux';

const rootReducer = combineReducers({
  menuReducer,
});

export type RootState = ReturnType<typeof rootReducer>;
export const store = createStore(rootReducer, devToolsEnhancer({}));
