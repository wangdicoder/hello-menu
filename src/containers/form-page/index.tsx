import React, { FormEvent } from 'react';
import './form-page.scss';
import { useHistory, useLocation } from 'react-router-dom';
import { MenuEntity } from '../../models/menu.model';

type Props = {
  /** Parent menu info */
  parentMenu: MenuEntity;
};

/**
 * Form page
 */
const FormPage = ({ parentMenu }: Props) => {
  const history = useHistory();
  const location = useLocation();

  const onCancel = () => {
    const idx = location.pathname.lastIndexOf('/');
    const prevPath = location.pathname.substr(0, idx);
    history.push(prevPath);
  };

  const onSubmit = (e: FormEvent) => {
    e.preventDefault();
    alert('You submitted the form.');
  };

  return (
    <div className="form-page">
      <h4 className="form-page__title">This is a form page from {parentMenu.name}</h4>
      <form onSubmit={onSubmit} className="form-page__form">
        <input className="form-page__input" placeholder="Name" />
        <input className="form-page__input" placeholder="Title" />
        <button type="submit" name="submit" className="form-page__btn form-page__submit-btn">
          Submit
        </button>
        <button type="button" name="cancel" onClick={onCancel} className="form-page__btn">
          Cancel
        </button>
      </form>
    </div>
  );
};

export default FormPage;
