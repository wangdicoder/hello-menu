/**
 * @author Di Wang
 */
import React from 'react';
import { render } from 'enzyme';
import { MemoryRouter } from 'react-router-dom';
import FormPage from '../index';

jest.mock('react-redux', () => ({
  useSelector: jest.fn(),
  useDispatch: jest.fn(),
}));

describe('<FormPage />', () => {
  it('should match the snapshot', () => {
    const menu = {
      id: '0',
      name: 'Menu 1',
      path: '/menu1',
      type: 'menu' as 'menu',
      parentId: null,
      children: null,
    };
    const wrapper = render(
      <MemoryRouter>
        <FormPage parentMenu={menu} />
      </MemoryRouter>
    );
    expect(wrapper).toMatchSnapshot();
  });
});
