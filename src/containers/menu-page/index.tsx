/**
 * @author Di Wang
 */
import React, { useEffect, useState } from 'react';
import { useRouteMatch, Route, Switch } from 'react-router-dom';
import axios from 'axios';
import { useDispatch, useSelector } from 'react-redux';
import { MenuEntity } from '../../models/menu.model';
import { API } from '../../apis';
import { RootState } from '../../store';
import { updateMenuTree } from '../../redux/menu.redux';
import { updateTreeItemChildren, getTreeChildren } from '../../utils/tree';
import Menu from '../../components/menu';
import Content from '../../components/content';

type Props = {
  /** Parent menu info */
  parentMenu?: MenuEntity;
};

/**
 * Render a menu page
 * @param parentMenu
 * @constructor
 */
const MenuPage = ({ parentMenu }: Props) => {
  const { url } = useRouteMatch();
  const menuTree = useSelector((state: RootState) => state.menuReducer.menuTree);
  const dispatch = useDispatch();
  // Current page's menus
  const [currMenus, setCurrMenus] = useState<MenuEntity[]>([]);

  /**
   * Handle fetched menu data
   *
   * 1. update menu tree's children
   * 2. update local state
   */
  const handleMenuDate = (fetchedMenus: MenuEntity[]) => {
    let menus: MenuEntity[];
    let updatedMenuTree: MenuEntity[];
    if (parentMenu) {
      menus = fetchedMenus.map((menu, idx) =>
        Object.assign({}, menu, { id: `${parentMenu.id}-${idx}`, parent: parentMenu.id })
      );
      const updatedParentMenu = Object.assign({}, parentMenu, { children: menus });
      updatedMenuTree = updateTreeItemChildren(menuTree, updatedParentMenu);
    } else {
      menus = fetchedMenus.map((menu, idx) =>
        Object.assign({}, menu, { id: `${idx}`, parent: null, children: null })
      );
      updatedMenuTree = menus;
    }
    dispatch(updateMenuTree(updatedMenuTree));
    setCurrMenus(menus);
  };

  /**
   * Get menu data
   */
  const getMenuData = async () => {
    // If the current menu level already exists in the menu tree,
    // get it from menu tree directly without fetching new menu items again.
    const menus = parentMenu && getTreeChildren(menuTree, parentMenu.id);
    if (menus) {
      setCurrMenus(menus);
    } else {
      try {
        const res = await axios.get(API.getNextMenus());
        const menus = res.data as MenuEntity[];
        handleMenuDate(menus);
      } catch (e) {
        console.log(e);
      }
    }
  };

  useEffect(() => {
    getMenuData();
  }, []);

  return (
    <div className="menu-page">
      <Menu dataSource={currMenus} url={parentMenu ? url : undefined} />
      <hr />
      <Switch>
        {currMenus.map((menu: MenuEntity) => (
          <Route key={menu.id} path={parentMenu ? `${url}${menu.path}` : menu.path}>
            <Content menu={menu} />
          </Route>
        ))}
      </Switch>
    </div>
  );
};

export default MenuPage;
