import React from 'react';
import { render } from 'enzyme';
import { MemoryRouter } from 'react-router-dom';
import MenuPage from '../index';

jest.mock('react-redux', () => ({
  useSelector: jest.fn(),
  useDispatch: jest.fn(),
}));

describe('<MenuPage />', () => {
  it('should match the snapshot', () => {
    const menu = {
      id: '0',
      name: 'Menu 1',
      path: '/menu1',
      type: 'menu' as 'menu',
      parentId: null,
      children: null,
    };
    const wrapper = render(
      <MemoryRouter>
        <MenuPage parentMenu={menu} />
      </MemoryRouter>
    );
    expect(wrapper).toMatchSnapshot();
  });
});
