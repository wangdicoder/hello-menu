/**
 * @author Di Wang
 */
import { MenuEntity } from '../models/menu.model';

export const UPDATE_MENU_TREE = 'UPDATE_MENU_TREE';

export interface MenuState {
  menuTree: MenuEntity[];
}

const initialState: MenuState = {
  menuTree: [],
};

interface UpdateMenuTreeAction {
  menuTree: MenuEntity[];
  type: typeof UPDATE_MENU_TREE;
}

export type MenuActionTypes = UpdateMenuTreeAction;

/**
 * Update the menu tree
 * @param menuTree
 */
export function updateMenuTree(menuTree: MenuEntity[]): MenuActionTypes {
  return {
    menuTree,
    type: UPDATE_MENU_TREE,
  };
}

/**
 * Menu Reducer
 * @param state
 * @param action
 */
export default function menuReducer(state = initialState, action: MenuActionTypes) {
  switch (action.type) {
    case 'UPDATE_MENU_TREE':
      return {
        ...state,
        menuTree: action.menuTree,
      };

    default:
      return state;
  }
}
