/**
 * @author Di Wang
 */
import MenuReducer, { updateMenuTree, UPDATE_MENU_TREE } from '../menu.redux';
import { MenuEntity } from '../../models/menu.model';

describe('Menu Redux Actions', () => {
  it('should create an action to update the tree', () => {
    const dataSource: MenuEntity[] = [
      {
        id: '0',
        name: 'Menu 1',
        path: '/menu1',
        type: 'menu',
        parentId: null,
        children: null,
      },
    ];
    const expectedAction = {
      type: UPDATE_MENU_TREE,
      menuTree: dataSource,
    };
    expect(updateMenuTree(dataSource)).toEqual(expectedAction);
  });
});

describe('Menu Reducer', () => {
  it('should return the correct state', () => {
    const dataSource: MenuEntity[] = [
      {
        id: '0',
        name: 'Menu 1',
        path: '/menu1',
        type: 'menu',
        parentId: null,
        children: null,
      },
    ];
    expect(MenuReducer(undefined, updateMenuTree(dataSource))).toEqual({
      menuTree: dataSource,
    });
  });
});
