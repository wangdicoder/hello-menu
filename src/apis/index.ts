/**
 * @author Di Wang
 */

/**
 * API class
 * The dummy json files are in the `public` folder.
 */
export class API {
  /**
   * Fetch the menu list for the first open the page
   */
  static getInitialMenus() {
    return `/api/menu-list-1.json`;
  }

  /**
   * Randomly fetch the menu list
   */
  static getNextMenus() {
    const id = Math.floor(Math.random() * 5) + 1;
    return `/api/menu-list-${id}.json`;
  }
}
