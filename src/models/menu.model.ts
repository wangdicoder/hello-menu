/**
 * @author Di Wang
 */
export type MenuEntity = {
  id: string;
  name: string;
  path: string;
  type: 'menu' | 'form' | 'null';
  parentId: string | null;
  children: MenuEntity[] | null;
};
