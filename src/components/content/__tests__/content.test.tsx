import React from 'react';
import { render, mount } from 'enzyme';
import { MemoryRouter } from 'react-router-dom';
import Content from '../index';

jest.mock('react-redux', () => ({
  useSelector: jest.fn(),
  useDispatch: jest.fn(),
}));

describe('<Content />', () => {
  it('should match the snapshot', () => {
    const menu = {
      id: '0',
      name: 'Menu 1',
      path: '/menu1',
      type: 'menu' as 'menu',
      parentId: null,
      children: null,
    };
    const wrapper = render(
      <MemoryRouter>
        <Content menu={menu} />
      </MemoryRouter>
    );
    expect(wrapper).toMatchSnapshot();
  });

  it('should render Form Page', () => {
    const menu = {
      id: '0',
      name: 'Menu 1',
      path: '/menu1',
      type: 'form' as 'form',
      parentId: null,
      children: null,
    };
    const wrapper = mount(
      <MemoryRouter>
        <Content menu={menu} />
      </MemoryRouter>
    );
    expect(wrapper.find('.form-page').length).toBe(1);
  });

  it('should render Menu Page', () => {
    const menu = {
      id: '0',
      name: 'Menu 1',
      path: '/menu1',
      type: 'menu' as 'menu',
      parentId: null,
      children: null,
    };
    const wrapper = mount(
      <MemoryRouter>
        <Content menu={menu} />
      </MemoryRouter>
    );
    expect(wrapper.find('.menu-page').length).toBe(1);
  });
});
