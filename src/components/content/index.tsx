import React from 'react';
import FormPage from '../../containers/form-page';
import MenuPage from '../../containers/menu-page';
import { MenuEntity } from '../../models/menu.model';

type Props = {
  menu: MenuEntity;
};

/**
 * Content Component. Page container
 *
 * According to the menu type from "menu" props, it will return different pages.
 */
const Content = ({ menu }: Props) => {
  const { type } = menu;
  switch (type) {
    case 'menu':
      return <MenuPage parentMenu={menu} />;

    case 'form':
      return <FormPage parentMenu={menu} />;

    default:
      return null;
  }
};

export default Content;
