import React from 'react';
import { render } from 'enzyme';
import { MemoryRouter } from 'react-router-dom';
import Menu from '../index';
import { MenuEntity } from '../../../models/menu.model';

describe('<Menu />', () => {
  const dataSource: MenuEntity[] = [
    {
      id: '0',
      name: 'Menu 1',
      path: '/menu1',
      type: 'menu',
      parentId: null,
      children: null,
    },
    {
      id: '1',
      name: 'Menu 2',
      path: '/menu2',
      type: 'null',
      parentId: null,
      children: null,
    },
  ];

  const wrapper = render(
    <MemoryRouter>
      <Menu dataSource={dataSource} />
    </MemoryRouter>
  );

  it('should match the snapshot', () => {
    expect(wrapper).toMatchSnapshot();
  });

  it('should render menu item correctly', () => {
    expect(wrapper.find('.menu__item').length).toBe(2);
  });

  it('should render link correctly', () => {
    expect(wrapper.find('.menu__link').first().text()).toBe('Menu 1');
  });

  it('should render non-function span correctly', () => {
    expect(wrapper.find('.menu__non-func-link').first().text()).toBe('Menu 2');
  });

  it('should match the correct link when parsing url', () => {
    const wrapper = render(
      <MemoryRouter>
        <Menu dataSource={dataSource} url="/root" />
      </MemoryRouter>
    );
    expect(wrapper.find('.menu__link').first().attr('href')).toBe('/root/menu1');
  });
});
