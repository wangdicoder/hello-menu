import React from 'react';
import './menu.scss';
import { MenuEntity } from '../../models/menu.model';
import { NavLink } from 'react-router-dom';

type Props = {
  /** Menu info **/
  dataSource: MenuEntity[];
  /** Path prefix */
  url?: string;
};

/**
 * Menu Component
 *
 * If the type of one menu item is "null", it will render a <span/> tag.
 */
const Menu = ({ dataSource, url }: Props) => (
  <ul className="menu">
    {dataSource.map(({ id, type, path, name }) => (
      <li key={id} className="menu__item">
        {type === 'null' ? (
          <span
            className="menu__non-func-link"
            onClick={() => alert('This page is under development.')}>
            {name}
          </span>
        ) : (
          <NavLink
            to={url ? `${url}${path}` : path}
            className="menu__link"
            activeClassName="menu__link_active">
            {name}
          </NavLink>
        )}
      </li>
    ))}
  </ul>
);

export default Menu;
