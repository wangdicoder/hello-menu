# Hello Menu

## How to run

Firstly,

```bash
$ npm i
```

or 

```bash
$ yarn install
```

Then,

```bash
$ npm start
```

### Run Test

```bash
$ npm run test
```

## Problem Thinking

This question is aiming to build an unlimited menu page. The menu list actually is only fetched page by page rather than get them all in once.
As a result, we need to figure out a data structure to store the menu list. 

I am using an m-ary tree structure. Each node has parent and children information, which can organise a huge tree. At any tree node, it can be easier
to get its parent node and children nodes. The id naming pattern is like "x-x-x-x", which will be easy to identify which level the node is.

So, what the expectation of this solution is that you can click any level menus. If the menu already fetched, the page just displays them directly.
Otherwise, the page will get a new menu list. This means that fetched menus are fixed, and will never be changed.


## Out of scopes

- Refresh the page, and the menu information is also retained.
